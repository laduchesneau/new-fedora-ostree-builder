# This is a justfile. See https://github.com/casey/just
# This is only used for local development. The builds made on the Fedora
# infrastructure are run in Pungi.

# Set a default for some recipes
default_image := "fedora-ostree"
# Default to unified compose now that it works for Silverblue & Kinoite builds
unified_core := "true"
# unified_core := "false"
force_nocache := "true"
# force_nocache := "false"

# Output the processed manifest for a given variant (defaults to Silverblue)
manifest image=default_image:
    #!/bin/bash
    set -euxo pipefail
    rpm-ostree compose tree --print-only --repo=repo {{image}}.yaml

# Clean up everything
clean-all:
    just clean-repo
    just clean-cache
    just clean-archive

# Only clean the ostree repo
clean-repo:
    rm -rf ./repo

# Only clean the package and repo caches
clean-cache:
    rm -rf ./cache

# Only clean the package and repo caches
clean-archive:
    rm -rf *.ociarchive

# Compose an OCI image
compose-image image=default_image:
    #!/bin/bash
    set -euxo pipefail
    image={{image}}

    ./ci/validate > /dev/null || (echo "Failed manifest validation" && exit 1)

    just prep

    build_date="$(date '+%Y%m%d')"
    timestamp="$(date --iso-8601=sec)"
    echo "${build_date}" > .buildid

    # TODO: Pull latest build for the current release
    # ostree pull ...


    echo "Composing ${image} ${CI_COMMIT_BRANCH}.${build_date} ..."
    # To debug with gdb, use: gdb --args ...

    ARGS="--cachedir=cache --initialize"
    if [[ {{force_nocache}} == "true" ]]; then
        ARGS+=" --force-nocache"
    fi
    CMD="rpm-ostree"
    if [[ ${EUID} -ne 0 ]]; then
        SUDO="sudo rpm-ostree"
    fi

    ${CMD} compose image ${ARGS} \
         --label="quay.expires-after=4w" \
        "${image}.yaml" \
        "${image}.ociarchive" \
            |& tee "logs/${image}_${CI_COMMIT_BRANCH}_${build_date}.${timestamp}.log"

# Preparatory steps before starting a compose. Also ensure the ostree repo is initialized
prep:
    #!/bin/bash
    set -euxo pipefail

    mkdir -p repo cache logs
    if [[ ! -f "repo/config" ]]; then
        pushd repo > /dev/null || exit 1
        ostree init --repo . --mode=archive
        popd > /dev/null || exit 1
    fi
    # Set option to reduce fsync for transient builds
    ostree --repo=repo config set 'core.fsync' 'false'

upload-container image=default_image:
    #!/bin/bash
    set -euxo pipefail
    image={{image}}

    if [[ -z ${QUAY_CI_REGISTRY_USER+x} ]] || [[ -z ${QUAY_CI_REGISTRY_PASSWORD+x} ]]; then
        echo "Skipping artifact archiving: Not in CI"
        exit 0
    fi

    if [[ "${CI}" != "true" ]]; then
        echo "Skipping artifact archiving: Not in CI"
        exit 0
    fi

    build_date="$(date '+%Y%m%d')"
    
    skopeo login --username "${QUAY_CI_REGISTRY_USER}" --password "${QUAY_CI_REGISTRY_PASSWORD}" "${QUAY_CI_REGISTRY}"
    # Copy fully versioned tag (major version, build date/id, git commit)
    skopeo copy --retry-times 3 "oci-archive:${image}.ociarchive" "docker://"${QUAY_CI_REGISTRY}"/${image}:${CI_COMMIT_BRANCH}.${build_date}.${CI_COMMIT_SHORT_SHA}.${CI_PIPELINE_ID}"
    # Update "un-versioned" tag (only major version)
    skopeo copy --retry-times 3 "oci-archive:${image}.ociarchive" "docker://"${QUAY_CI_REGISTRY}"/${image}:${CI_COMMIT_BRANCH}"
    skopeo logout "${QUAY_CI_REGISTRY}"
