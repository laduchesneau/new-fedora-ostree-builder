# Experimental Ostree Native Container images for rpm-ostree based Fedora desktop / Silverblue variant

My personal Fedora image builder.

## Overview

This repo is based on [gitlab.com/fedora/ostree/ci-test](https://gitlab.com/fedora/ostree/ci-test) wich is a fork of
[pagure.io/workstation-ostree-config](https://pagure.io/workstation-ostree-config)
with CI and minor changes on top to enable us to build experiemental Ostree
Native Container images for rpm-ostree based Fedora desktop variant.

The offcial upstream sources for Fedora Silverblue ([Fedora Change](https://fedoraproject.org/wiki/Changes/Fedora_Sway_Spin))
remain at [pagure.io/workstation-ostree-config](https://pagure.io/workstation-ostree-config)
and the official builds are only available from the Fedora ostree repo for now.

See [pagure.io/releng/issue/11047](https://pagure.io/releng/issue/11047) for
the progress to mirror the official builds into container images.

See also the tracking issue for Silverblue:
[github.com/fedora-silverblue/issue-tracker/issues/359](https://github.com/fedora-silverblue/issue-tracker/issues/359).


## Images built

This project builds the following images:

- Fedora OSTree (non extra tag):
    - Unofficial build based on the official Silverblue variant
    - Latest Fedora stable
    - [https://quay.io/repository/ladsu/fedora-ostree](https://quay.io/repository/ladsu/fedora-ostree?tab=tags)
    - Only contains packages from Fedora repos

- Fedora OSTree (with extra tag):
    - Unofficial build based on the official Silverblue variant
    - Latest Fedora stable
    - [https://quay.io/repository/ladsu/fedora-ostree](https://quay.io/repository/ladsu/fedora-ostree?tab=tags)
    - Extra tag contains Canon printer drivers, Proton Mail Bridge, Gnome Extensions and TPM2 Enable and more


