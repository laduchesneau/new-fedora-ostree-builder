#!/bin/bash

print_info(){
    printf "%s\n" "   $1"
}

print_warning(){
    printf '\e[33m%s\e[0m\n' "   $1"
}

print_error(){
    printf '\e[31m%s\e[0m\n' "   $1"
}

print_success(){
    printf '\e[32m%s\e[0m\n' "  $1"
}


current_hostname=$(hostname -s)
if cat /sys/devices/virtual/dmi/id/board_vendor >/dev/null 2>&1; then
    motherboard_vendor=$(cat /sys/devices/virtual/dmi/id/board_vendor)
fi
if cat /sys/devices/virtual/dmi/id/board_name >/dev/null 2>&1; then
    motherboard_name=$(cat /sys/devices/virtual/dmi/id/board_name)
fi

if [[ $motherboard_vendor == "ASUSTeK COMPUTER INC." && $motherboard_name == "TUF GAMING X570-PRO (WI-FI)" ]]; then
    # Primary Workstation
    hostname="ws01"
elif [[ $motherboard_vendor == "Framework" && $motherboard_name == "FRANMACP06" ]]; then
    # Laptop
    hostname="ws02"
    # Verify if hid sendor is disabled
    if ! grep "hid_sensor_hub" /proc/cmdline >/dev/null 2>&1; then
        read -p "Blacklist Framework HID sendor ? (Y/N): " confirm 
        if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
            print_warning "sudo is needed to blacklist hid_sensor_hub"
            sudo grubby --update-kernel=ALL --args="module_blacklist=hid_sensor_hub"
        fi
        unset $confirm
    fi 
else
    print_error "could not find motherboard"
fi

if [[ -n "$hostname" ]]; then
    if [[ $hostname -ne $current_hostname ]]; then
        read -p "Set Hostname? (Y/N): " confirm 
        if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
            read -p "Hostname: " hostname 
            hostnamectl hostname $hostname
        fi
        unset $confirm
    else
        print_success "hostname correctly set"
    fi
fi

# Remove Fedora flatpak
if flatpak remotes | grep -i fedora >/dev/null 2>&1; then
    read -p "Delete Fedora Flatpak repository? (Y/N): " confirm 
    if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
        app_list=$(flatpak list --app --columns=origin,application | awk -F"\t" '{ if ($1 =="fedora") print $2 }')
        while IFS= read -r app_id; do
            flatpak uninstall --assumeyes --noninteractive --delete-data $app_id
        done <<< "$app_list"
        flatpak uninstall --assumeyes --noninteractive --delete-data org.fedoraproject.Platform
        print_warning "sudo is needed to remove flatpak remotes"
        sudo flatpak remote-delete fedora
        sudo flatpak remote-delete fedora-testing
    fi
    unset $confirm
else
    print_info "Fedora flatpak repository not installed"
fi

# Add Flathub
if ! flatpak remotes | grep -i flathub >/dev/null 2>&1; then
    read -p "Add Flathub Flatpak repository? (Y/N): " confirm 
    if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
        print_warning "sudo is needed to add flatpak remotes"
        sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
    fi
    unset $confirm
else
    print_info "Flathub already repo installed"
fi

flatpak install --assumeyes --noninteractive --or-update flathub \
    org.fedoraproject.MediaWriter \
    org.gnome.baobab \
    org.gnome.Calculator \
    org.gnome.Calendar \
    org.gnome.Characters \
    org.gnome.Connections \
    org.gnome.eog \
    org.gnome.Evince \
    org.gnome.font-viewer \
    org.gnome.Logs \
    org.gnome.Maps \
    org.gnome.TextEditor \
    org.gnome.Weather \
    org.gtk.Gtk3theme.Adwaita-dark \
    org.libreoffice.LibreOffice \
    com.belmoussaoui.Authenticator \
    com.calibre_ebook.calibre \
    com.discordapp.Discord \
    com.github.micahflee.torbrowser-launcher \
    com.github.tchx84.Flatseal \
    com.jgraph.drawio.desktop \
    com.makemkv.MakeMKV \
    com.mattjakeman.ExtensionManager \
    com.valvesoftware.Steam \
    com.vscodium.codium \
    im.riot.Riot \
    io.github.TransmissionRemoteGtk \
    io.github.chris2511.xca \
    io.mpv.Mpv \
    io.podman_desktop.PodmanDesktop \
    net.mediaarea.MediaInfo \
    org.bunkus.mkvtoolnix-gui \
    org.mozilla.firefox \
    org.mozilla.Thunderbird \
    org.raspberrypi.rpi-imager \
    org.wireshark.Wireshark  \
    tv.kodi.Kodi \
    ;

if flatpak override --show --user org.mozilla.firefox | grep MOZ_ENABLE_WAYLAND >/dev/null 2>&1; then
    flatpak override --user --env=MOZ_ENABLE_WAYLAND=1 org.mozilla.firefox
fi
if flatpak override --show --user org.mozilla.Thunderbird | grep MOZ_ENABLE_WAYLAND >/dev/null 2>&1; then
    flatpak override --user --env=MOZ_ENABLE_WAYLAND=1 org.mozilla.Thunderbird
fi

